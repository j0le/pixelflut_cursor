﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;

namespace Pixelflut
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Struct representing a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            //public static implicit operator Point(POINT point)
            //{
            //    return new Point(point.X, point.Y);
            //}
        }

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <see>See MSDN documentation for further information.</see>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        public static POINT GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);
            //bool success = User32.GetCursorPos(out lpPoint);
            // if (!success)

            return lpPoint;
        }

        private readonly POINT SIZE = new POINT() {X= 1920, Y= 1080 };
        const int TCP_PACK_SIZE = 1460;
        const int SQUARE_SIZE = 50;

        byte[] buffer = new byte[TCP_PACK_SIZE];
        int buf_size = 0;
        POINT send_pos;
        private object send_pos_lock = new object();
        private IPAddress ip = null;


        private List<Thread> thread_list = new List<Thread>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //generate_buf();
            load_cursor();
            await update_position();
        }

        private async Task update_position()
        {
            POINT last_pos = new POINT();
            POINT send_pos_not_trans = new POINT();
            int counter = 0;
            while (true)
            {
                POINT pos = GetCursorPosition();
                if(pos.X != last_pos.X || pos.Y != last_pos.Y)
                {
                    //generate_new_buf(pos);
                    this.tb_x_pos.Text = pos.X.ToString();
                    this.tb_y_pos.Text = pos.Y.ToString();
                }
                last_pos = pos;
                double distance = Math.Sqrt((pos.X - send_pos_not_trans.X) * (pos.X - send_pos_not_trans.X) + (pos.Y - send_pos_not_trans.Y) * (pos.Y - send_pos_not_trans.Y));
                if (/*distance > 200 &&*/ counter == 0)
                {
                    send_pos_not_trans = pos;
                    POINT temp = new POINT()
                    {
                        X = pos.X * SIZE.X / 1366,
                        Y = pos.Y * SIZE.Y / 768
                    };
                    lock (send_pos_lock)
                        send_pos = temp;
                    this.tb_x_pos_send.Text = send_pos.X.ToString();
                    this.tb_y_pos_send.Text = send_pos.Y.ToString();
                }
                await Task.Delay(10);
                counter = (counter + 1) % 3;
            }
        }

        private void btn_start_pf_Click(object sender, RoutedEventArgs e)
        {
            btn_start_pf.IsEnabled = false;
            IPAddress ip;
            if(!IPAddress.TryParse(tbx_ip.Text, out ip))
            {
                MessageBox.Show("Wrong IP");
                return;
            }
            this.ip = ip;

            addThread(1, 7);
            addThread(3, 4);
            addThread(6, 2);

            void addThread(int iterations, int n = 1)
            {
                for(int i =0; i < 6; ++i)
                {
                    var t = new Thread(() => { spam(ip, iterations); });
                    thread_list.Add(t);
                    t.Start();
                }
            }
        }


        //private void generate_buf()
        //{
        //    //pos.X = pos.X * SIZE.X / 1366;
        //    //pos.Y = pos.Y * SIZE.Y / 768;
        //    POINT pos = new POINT() { X = 0, Y = 0 };
        //    if (pos.X < 0 || pos.Y < 0)
        //    {
        //        MessageBox.Show("Error");
        //        return;
        //    }
        //    int square_size = 50;
        //    using (MemoryStream mStream = new MemoryStream(square_size*square_size*20))
        //    {
        //        bool fill_buff = true;
        //        int bytes_writen = 0;
        //        //while(fill_buff)
        //            for (int i =0; i < square_size && fill_buff; ++i)
        //            {
        //                int x = i + pos.X;
        //                if (x > SIZE.X)
        //                    break;

        //                for(int j = 0; j < square_size; ++j)
        //                {
        //                    int y = j + pos.Y;
        //                    if (y > SIZE.X)
        //                        break;

        //                    byte[] new_bytes = Encoding.ASCII.GetBytes($"PX {x} {y} 33ff00\n");
        //                    //if(bytes_writen+new_bytes.Length> TCP_PACK_SIZE)
        //                    //{
        //                    //    for (; bytes_writen < TCP_PACK_SIZE; ++bytes_writen)
        //                    //        mStream.WriteByte(0x20);//SPACE
        //                    //    fill_buff = false;
        //                    //    break;
        //                    //}
        //                    mStream.Write(new_bytes, 0, new_bytes.Length);
        //                    bytes_writen += new_bytes.Length;
        //                }
        //            }
        //        if (bytes_writen != mStream.Position)
        //            throw new Exception("fatal");
        //        buf_size = (int)mStream.Position; 
        //        buffer = mStream.GetBuffer();
        //    }
        //}

        private void spam(IPAddress ip, int iterations = 3, bool endless = true)
        {
            bool redo;
            do
            {
                redo = false;
                try
                {
                    using (TcpClient client = new TcpClient())
                    {
                        client.Connect(new System.Net.IPEndPoint(ip, 1234));
                        using (var stream = client.GetStream())
                        {
                            POINT my_send_pos = new POINT();
                            do
                            {
                                POINT temp_send_pos;
                                lock (send_pos_lock)
                                    temp_send_pos = send_pos;

                                if(temp_send_pos.X != my_send_pos.X || temp_send_pos.Y != my_send_pos.Y)
                                {
                                    my_send_pos = temp_send_pos;
                                    send_offset(stream, my_send_pos);
                                }

                                for (int j = 0; j < iterations; ++j)
                                {
                                    stream.Write(buffer, 0, buf_size);
                                    //stream.Flush();
                                }
                            } while (endless) ;
                        }
                    }
                }
                catch(SocketException ex)
                {
                    redo = true;
                    if (ex.SocketErrorCode == SocketError.TimedOut)
                        Thread.Sleep(10);
                    else
                        throw;//temporary throw
                }
                catch (IOException ex)
                {
                    if (ex.InnerException is SocketException sEx
                        && (sEx.SocketErrorCode == SocketError.ConnectionReset
                        || sEx.SocketErrorCode == SocketError.ConnectionAborted))
                    {
                        redo = true;
                    }
                    else
                        throw;
                }
            } while (redo && !endless);
        }


        private void send_offset(Stream stream, POINT offset)
        {
            byte[] data = Encoding.ASCII.GetBytes($"OFFSET {offset.X} {offset.Y}\n");
            stream.Write(data, 0, data.Length);
        }

        private void load_cursor()
        {//16 * 25
            var bitmap = Properties.Resources.Cursor;

            using(MemoryStream mStream = new MemoryStream(16*25*20))
            {
                for(int x = 0; x < 16; ++x)
                {
                    for(int y = 0; y < 25; ++y)
                    {
                        var color = bitmap.GetPixel(x, y);
                        //is red
                        if (color.R == 0xff && color.G == 0 && color.B == 0)
                            continue;

                        byte[] new_bytes = Encoding.ASCII.GetBytes($"PX {x} {y} {color.R.ToString("X2")}{color.G.ToString("X2")}{color.B.ToString("X2")}\n");
                        mStream.Write(new_bytes, 0, new_bytes.Length);
                    }
                }

                buf_size = (int)mStream.Position; 
                buffer = mStream.GetBuffer();
            }
        }

        //private void generate_action_buf()
        //{
        //    var bitmap = Properties.Resources.Cursor;

        //    using(MemoryStream mStream = new MemoryStream(16*25*20))
        //    {
        //        for(int x = 0; x < 16; ++x)
        //        {
        //            for(int y = 0; y < 25; ++y)
        //            {
        //                var color = bitmap.GetPixel(x, y);
        //                //is red
        //                if (color.R == 0xff && color.G == 0 && color.B == 0)
        //                    continue;

        //                byte[] new_bytes = Encoding.ASCII.GetBytes($"PX {x} {y} {color.R.ToString("X2")}{color.G.ToString("X2")}{color.B.ToString("X2")}\n");
        //                mStream.Write(new_bytes, 0, new_bytes.Length);
        //            }
        //        }

        //        buf_size = (int)mStream.Position; 
        //        buffer = mStream.GetBuffer();
        //    }

        //}

        //bool key_gesperrt = false;
        //private async Task Window_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (key_gesperrt)
        //        return;
        //    key_gesperrt = true;

        //    if (e.Key == Key.Space)
        //    {
        //        await Task.Run(async delegate ()
        //        {
        //            try
        //            {
        //                using(var client = new TcpClient())
        //                {
        //                    client.Connect(new IPEndPoint(ip, 1234));

        //                    using(var stream = client.GetStream())
        //                    {
        //                        POINT my_send_pos;
        //                        lock (send_pos_lock)
        //                            my_send_pos = send_pos;

        //                        send_offset(stream, my_send_pos);

        //                        await stream.WriteAsync(action_buf, 0, action_buf_size);
        //                    }
        //                }
        //            }
        //            catch(SocketException)
        //            { }
        //            catch (IOException ex)
        //                when (ex.InnerException is SocketException)
        //            { }
                    
        //        });
        //    }

        //    await Task.Delay(200);
        //    key_gesperrt = false;
        //}
    }
}
